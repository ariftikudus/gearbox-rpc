## [0.7.0] - 2021-10-19

General:
    - Added/changed/simplified/clarified several logger contexts
RPC:
    - Improved documentation and auth wrappers
Library:
    - Moved RPC authorization utilities to their own module (still imported into utils)
    - Removed LogWriter class template because its existence was a bad idea 
Daemon:
    - Refactored init process
    - Decoupled cog manager from cog scheduler
    - Moved some setup logic from daemon core to cog manager
    - Moved log buffer server to its own file
    - Fixed major crash condition in scheduler affecting versions 0.5.0 and 0.6.0

---

## [0.6.0] - 2021-10-18

RPC:
    - Made authorization strings case insensitive
    - Decoupled RPC interface from API
    - Added error string field to all response payloads

---

## [0.5.0] - 2021-10-16

General:
    - Switched to using package-relative imports where possible
    - Added packages path to system path so that cog packages can be directly imported or registered as Django apps by name
    - Handling added to all components for failure condition where gRPC modules are not built
WebUI:
    - Renamed from gearbox_django to gearboxweb
Library:
    - Renamed from gearbox to gearboxlib
RPC:
    - Now an optional (though highly recommended) component
Daemon:
    - Overhauled cog scheduler so it no longer ties up threads
    - Moved threadpool management to manager class

---

## [0.4.0] - 2021-10-16

General:
    - Adopted shed for code formatting
Daemon:
    - Cog config files are now cog package manifests that contain cog configs as well as package metadata
    - Cog packages can now depend on other packages, specific package versions, and/or specific GearBox versions

---

## [0.3.0] - 2021-10-10

WebUI:
    - Fancy logo!
    - Complete overhaul of layout, content, and controls
    - Added authentication
    - Graph view showing how cogs are connected through models
    - Support for management of multiple RPC servers
    - Live logs and status monitoring
    - Semantic highlighting of cogs and their logs
RPC:
    - Configurable authorization HMAC algorithm
    - Added streaming endpoint for status and logs
    - Authorization now required for all commands
Daemon:
    - Injection of historical records into cogs
    - Simplified model definitions with default timestamp field and automatic idempotency_key property

---

## [0.2.0] - 2021-09-28

Complete rewrite, almost everything changed

---

## [0.1.0] - 2021-03-07

Added:
- All the things

Removed:
- Nothing from nothing is nothing

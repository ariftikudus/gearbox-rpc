let currentPage = "cogs"

function reload() {
    window.location.reload()
}

const parseCookie = (str =>
  str
    .split(';')
    .map(v => v.split('='))
    .reduce((acc, v) => {
      acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim());
      return acc;
    }, {}));

function setCookie(cookieName, cookieValue, nDays) {
    document.cookie = cookieName+"="+escape(cookieValue)+";";
}

function RPCOperation(scope, action, name=null) {
    let cookies = parseCookie(document.cookie)
    let rpc_host = cookies["rpc_host"]
    let path = '/'+scope+'/'+rpc_host+'/'+action+'/';
    if (name) {
        path = path+name+'/'
    }
    fetch(path)
        .then(response => response.text())
        .then(response => {
            setTimeout(Navigate, scope == "daemon" ? 2000 : 400);
            if (scope == "daemon") {
                setTimeout(Navigate, 2000);
                setTimeout(WatchStatus, 2000);
            } else {
                setTimeout(Navigate, 400);
            }
        })
        .catch(error => console.log('error', error));
}

function Navigate(path=null) {
    let cookies = parseCookie(document.cookie)
    let rpc_host = cookies["rpc_host"]
    if (path) {
        currentPage = path
    }
    fetch(currentPage+'/'+rpc_host)
        .then(response => response.text())
        .then(response => {
            content = document.getElementById("content");
            content.innerHTML = response;
            scripts = content.getElementsByTagName("script");
            if (scripts) {
                for (var i = 0; i < scripts.length; i++) {
                    eval(scripts[i].innerHTML);
                }
            }
        })
        .catch(error => console.log('error', error));
}

// let status_error_counter = 0
function WatchStatus(host) {
    fetch('/status/'+host+'/')
        .then(response => response.body)
        .then(body => {
            const reader = body.getReader();
            workers = document.getElementById('threadPoolWorkers');
            emitted = document.getElementById('dataEmitted');
            cpu = document.getElementById('cpuUtilization');
            log_target = document.getElementById('logs');
            workers.className = '';
            emitted.className = '';
            cpu.className = '';
            return new ReadableStream({
                start(controller) {
                    function push() {
                        reader.read().then( (response) => {
                            if (response.done) {
                                console.error(response.value);
                                // let close_message = document.createTextNode("Server closed stream, trying to reconnect...");
                                let close_message = document.createTextNode("Server closed stream");
                                let close_message_container = document.createElement("div");
                                close_message_container.appendChild(close_message);
                                log_target.appendChild(close_message_container);
                                close_message_container.scrollIntoView(false);
                                workers.classList.add("disconnected");
                                emitted.classList.add("disconnected");
                                cpu.classList.add("disconnected");
                                controller.close();
                                // setTimeout(WatchStatus, 400);
                                return;
                            }
                            // status_error_counter = 0
                            controller.enqueue(response.value);
                            let message = new TextDecoder().decode(response.value);
                            let rpc_message = JSON.parse(message);
                            workers.innerHTML = rpc_message.threadPoolWorkers ?? 0;
                            emitted.innerHTML = rpc_message.dataEmitted ?? 0;
                            cpu.innerHTML = rpc_message.cpuUtilization ?? 0;
                            if ("logs" in rpc_message) {
                                rpc_message.logs.forEach(appendLog);
                            }
                            push();
                        })
                    }
                    push();
                }
            });
        })
        .then(result => {
            console.log(result);
        })
}

function appendLog(log) {
    let cookies = parseCookie(document.cookie)
    let rpc_host = cookies["rpc_host"]
    log_target = document.getElementById('logs');
    let specific_context = log.context.substring(log.context.lastIndexOf(".") + 1);
    let message_element = document.createTextNode("[" + log.context + ":" + log.level + "] " + log.message + "\n");
    let timestamp_element = document.createTextNode(log.timestamp.substring(0, log.timestamp.lastIndexOf(".")));
    let message_container = document.createElement("div");
    let timestamp_container = document.createElement("div");
    let log_container = document.createElement("div");
    let floatfix = document.createElement("div");
    message_container.appendChild(message_element);
    timestamp_container.appendChild(timestamp_element);
    message_container.classList.add("context_" + specific_context);
    message_container.classList.add("logMessage");
    timestamp_container.classList.add("timestamp");
    floatfix.classList.add("floatfix");
    log_container.appendChild(message_container);
    log_container.appendChild(timestamp_container);
    log_container.appendChild(floatfix);
    log_container.classList.add(log.level);
    log_target.appendChild(log_container);
    timestamp_container.scrollIntoView(false);
    sessionStorage.setItem(rpc_host, log_target.innerHTML);
}

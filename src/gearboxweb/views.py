from django.conf import settings  # type: ignore
from django.contrib.auth.decorators import login_required  # type: ignore
from django.http import HttpRequest, HttpResponse, StreamingHttpResponse  # type: ignore
from django.shortcuts import render  # type: ignore

try:
    import grpc  # type: ignore
    from google.protobuf.json_format import MessageToDict, MessageToJson

    from gearboxd.rpc import gearbox_pb2, gearbox_pb2_grpc  # type: ignore
    from gearboxlib.utils import TITLE_SPLIT, authorize_command  # type: ignore
except ImportError:
    # Views will be overwritten at the end of the file
    pass


@login_required
def main(request: HttpRequest):
    host = current_rpc_host = request.COOKIES.get(
        "rpc_host", next(iter(settings.REMOTE_RPC_INTERFACES.keys()))
    )
    return _try_render(
        request,
        "ListCogs",
        host,
        "home.html",
        # Extra page context
        rpc_hosts=settings.REMOTE_RPC_INTERFACES.keys,
        current_rpc_host=host,
    )


@login_required
def graph(request: HttpRequest, host: str):
    return _try_render(request, "GraphCogs", host, "graph.html")


@login_required
def cogs(request: HttpRequest, host: str):
    return _try_render(request, "ListCogs", host, "cogs.html")


@login_required
def cog_action(request: HttpRequest, host: str, action: str, cog: str):
    result = _gearboxd_cog_state_change_request(host, action, cog)
    return HttpResponse(result.success)


@login_required
def daemon_reload(request: HttpRequest, host: str):
    result = _try_get_context(host, "ReloadDaemon")
    return HttpResponse(result.success)


@login_required
def watch_status(request: HttpRequest, host: str):
    return StreamingHttpResponse(_status_generator(host))


def _try_render(http_request, rpc_method, rpc_host, template, **extra_context):
    try:
        context = _as_dict(_try_get_context(rpc_host, rpc_method))
        context.update(**extra_context)
    except grpc.RpcError as e:
        context = {"error": f"RPC server error: {e.args[0].details}"}
    return render(http_request, template, context)


def _try_get_context(rpc_host, rpc_method, extra_context: dict = None):
    extra_context = extra_context or {}
    rpc_request = getattr(gearbox_pb2, f"{rpc_method}Request")
    # Extract a command and action from the method name for authorization
    command, target = TITLE_SPLIT.split(rpc_method)
    return _gearboxd_rpc_request(
        rpc_host,
        command,
        target,
        rpc_method,
        rpc_request,
    )


def _gearboxd_cog_state_change_request(host, action, target):
    kwargs = {"action": action, "name": target}
    return _gearboxd_rpc_request(
        host,
        action,
        target,
        "CogStateModify",
        gearbox_pb2.CogStateModifyRequest,
        kwargs,
    )


def _gearboxd_rpc_request(host, action, target, endpoint, request_type, kwargs=None):
    kwargs = kwargs or {}
    host_info = settings.REMOTE_RPC_INTERFACES.get(host)
    if not host_info:
        return "Invalid host specified"
    interface, secret = host_info
    with grpc.insecure_channel(interface) as channel:
        stub = gearbox_pb2_grpc.GearBoxStub(channel)
        rpc_endpoint = getattr(stub, endpoint)
        # Generate auth at last opportunity to maximize usability
        auth = authorize_command(action, target, secret)
        return rpc_endpoint(request_type(authorization=auth, **kwargs))


def _status_generator(host):
    for status in _gearboxd_rpc_stream_request(
        host, "watch", "status", "WatchStatus", gearbox_pb2.WatchStatusRequest
    ):
        yield _as_json(status)


def _gearboxd_rpc_stream_request(
    host, action, target, endpoint, request_type, kwargs=None
):
    kwargs = kwargs or {}
    host_info = settings.REMOTE_RPC_INTERFACES.get(host)
    if not host_info:
        return "Invalid host specified"
    interface, secret = host_info
    with grpc.insecure_channel(interface) as channel:
        stub = gearbox_pb2_grpc.GearBoxStub(channel)
        rpc_endpoint = getattr(stub, endpoint)
        # Generate auth at last opportunity to maximize usability
        auth = authorize_command(action, target, secret)
        yield from rpc_endpoint(request_type(authorization=auth, **kwargs))


def _as_json(gearboxd_object):
    try:
        return MessageToJson(gearboxd_object)
    except:
        return f'{"error": "Failed to parse RPC response: {gearboxd_object}"}'


def _as_dict(gearboxd_object):
    try:
        return MessageToDict(gearboxd_object)
    except:
        return {"error": f"Failed to parse RPC response: {gearboxd_object}"}


# Overwrite views if RPC server is not built
if "grpc" not in locals():

    def no_rpc(*args, **kwargs):
        return HttpResponse(
            "This instance of GearBox is headless. "
            "Please build the gRPC client in order to use the web interface."
        )

    main = graph = cogs = cog_action = daemon_reload = watch_status = no_rpc

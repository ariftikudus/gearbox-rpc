import os
import sys

import yaml
from django.core.management import execute_from_command_line  # type: ignore

from gearboxd.gearbox import GearBox  # type: ignore


def manage():
    execute_from_command_line(sys.argv[1:])


def server():
    sys.argv.pop(1)
    sys.argv.extend(("manage", "runserver"))
    execute_from_command_line(sys.argv[1:])


def makemigrations():
    sys.argv.pop(1)
    sys.argv.extend(("manage", "makemigrations"))
    execute_from_command_line(sys.argv[1:])


def migrate():
    sys.argv.pop(1)
    sys.argv.extend(("manage", "migrate"))
    execute_from_command_line(sys.argv[1:])


def daemon():
    gb = GearBox()
    gb.load()
    gb.run()


def configurecogs():
    from gearboxlib.cog_utils import (  # type: ignore
        find_packages,
        get_main_config,
        merge_cog_configs,
    )

    config_files = find_packages("packages")
    print(f"Loading {len(config_files)} cog config(s) and 1 main config")
    cog_configs = merge_cog_configs(config_files)
    main_config = get_main_config()
    config = {**cog_configs, **main_config}
    if config != main_config:
        with open("cog_config.yml", "w") as f:
            yaml.safe_dump(config, f)
        print("Merged into cog_config.yml - please populate values manually")
    else:
        print("No changes detected")


def grpc():
    if sys.argv[2] == "build":
        build_args = "python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. ./gearboxd/rpc/gearbox.proto"
        os.execlpe(sys.executable, *build_args.split(), os.environ)
    else:
        print("Unknown or missing grpc command - try 'build'")


commands = {
    "manage": manage,
    "server": server,
    "daemon": daemon,
    "configurecogs": configurecogs,
    "makemigrations": makemigrations,
    "migrate": migrate,
    "grpc": grpc,
}

if __name__ == "__main__":
    try:
        command = sys.argv[1]
    except IndexError:
        print(f"Please supply a run command: {', '.join(commands.keys())}")
        exit(1)
    try:
        c = commands[command]
    except KeyError:
        print(f"Invalid command - please choose one of: {', '.join(commands.keys())}")
    c()

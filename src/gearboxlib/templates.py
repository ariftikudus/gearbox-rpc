import logging
import os
from typing import Any, Dict, Generator, List

import yaml
from django.db import models  # type: ignore
from pubsub import pub  # type: ignore

from .models import CogMemory
from .utils import diff, hash_message  # type: ignore


class Cog:
    # What model topics does this subscribe to and/or produce?
    collects, emits = None, None
    # How often should this be run if it has a main() method?
    schedule = 1
    history: list = []
    _package = ""

    def __init__(self, *args, **kwargs):
        name = self.__class__.__name__
        context = ".".join((self._package, name))
        self.log = logging.getLogger(context.lower())
        super().__init__()

    def setup(self) -> bool:
        if config := self._get_config_keys():
            self.config = self._populate_config(config)
        if self.collects is not None:
            if isinstance(self.collects, tuple):
                for topic in self.collects:
                    self._subscribe(topic)
            else:
                self._subscribe(self.collects)
        return True

    def unsetup(self) -> None:
        self.config = {}
        if self.collects is not None:
            if isinstance(self.collects, tuple):
                for topic in self.collects:
                    self._unsubscribe(topic)
            else:
                self._unsubscribe(self.collects)

    def emit(self, data: models.base.ModelBase) -> None:
        """Publish a payload"""
        topic_name = data.__class__.__name__.lower()
        pub.sendMessage(topicName=topic_name, payload=data)

    def receive(self, payload: models.base.ModelBase) -> None:
        """Subscriber cogs should override this method"""
        self.log.debug(f"No handler for message: {payload}")

    def _receive(self, payload) -> None:
        """
        Wrapper for receive that filters out subscribed items that have already
        been received, e.g. during a previous run.
        """
        message_id = hash_message(payload)
        if message_id in self.history:
            return None
        self.receive(payload)
        self.history.append(message_id)
        CogMemory(cog=self.__class__.__name__, message_id=message_id).save()

    def _subscribe(self, topic) -> None:
        if isinstance(topic, models.base.ModelBase):
            topic = topic().__class__.__name__.lower()
        elif topic is not pub.ALL_TOPICS:
            self.log.error(f"Attempted to subscribe to unknown topic: {topic}")
            return
        pub.subscribe(self._receive, topic)
        self.log.debug(f"Subscribed to {topic}")

    def _unsubscribe(self, topic) -> None:
        if isinstance(topic, models.base.ModelBase):
            topic = topic().__class__.__name__.lower()
        elif topic is not pub.ALL_TOPICS:
            self.log.error(f"Attempted to unsubscribe from unknown topic: {topic}")
            return
        pub.unsubscribe(self._receive, topic)
        self.log.debug(f"Unsubscribed from {topic}")

    def _get_config_keys(self) -> List[str]:
        config_path = os.path.join("packages", self._package, "config.yml")
        if not os.path.isfile(config_path):
            return []
        config = yaml.safe_load(open(config_path)) or {}
        self_selector = self.__class__.__name__.lower() + "."
        return [
            str(x).replace(self_selector, "")
            for x in config.keys()
            if x.startswith(self_selector) or "." not in x
        ]

    def _populate_config(self, config_keys: list) -> dict:
        """
        Turns a list of configuration keys into a config dictionary with the
        appropriate values.

        Class inheritance is used to fill in unpopulated values with any
        available configuration elements from parent class(es).

        Parameters:
            config_keys (list): A list of keys to get configuration values for
        """
        config: Dict[str, Any] = {}
        try:
            main_config = yaml.safe_load(open("cog_config.yml")) or {}
        except:
            self.log.error("Unable to get config, please run configurecogs")
            return {}
        available_config_keys = main_config.keys()
        for key in diff(config_keys, config.keys()):
            # Try to find non-null value for key anywhere in config hierarchy
            for selector in self._generate_key_selectors(key):
                # Check if selector matches an available key
                if (
                    selector in available_config_keys
                    and main_config[selector] is not None
                ):
                    config[key] = main_config[selector]
                    # Exit loop on match
                    break
        # Check to ensure all config options have been defined
        unpopulated = diff(config_keys, config.keys())
        if unpopulated:
            self.log.error(f"Missing config values(s): {', '.join(unpopulated)}")
        return config

    def _generate_key_selectors(self, key: str) -> Generator[str, None, None]:
        base_classes = [
            x.__name__.lower()
            for x in type.mro(self.__class__)
            if x.__name__.lower() != "object"
        ]
        # Fully qualified key selectors for class and ancestor classes
        for base_class in base_classes:
            yield dot_join(self._package, base_class, key)
        # Key selectors for class and ancestor classes
        for base_class in base_classes:
            yield dot_join(base_class, key)
        # Key selector for package
        yield dot_join(self._package, key)
        # Global key selector
        yield key


def dot_join(*args: str) -> str:
    return ".".join(args)

from functools import cached_property
from uuid import uuid4

from django.db import models  # type: ignore


class Model(models.Model):
    ephemeral = False
    recorded = models.DateTimeField(auto_now=True)

    @cached_property
    def idempotency_key(self) -> str:
        if constraints := getattr(self.Meta, "constraints", None):
            fields = [self.__class__.__name__]
            # TODO: Handle constraints separately somehow
            for constraint in constraints:
                fields.extend([getattr(self, field) for field in constraint.fields])
            return "_".join(fields)
        elif self.ephemeral:
            return uuid4().hex
        else:
            return f"{self.__class__.__name__}_{self.pk}"

    class Meta:
        abstract = True


class CogMemory(Model):
    cog = models.CharField(max_length=128)
    message_id = models.CharField(max_length=56, primary_key=True)

    @property
    def idempotency_key(self):
        return f"{self.cog}_{self.message_id}"

import logging
import re
from datetime import datetime
from functools import wraps
from typing import Optional

from django.conf import settings  # type: ignore
from django.utils.crypto import (  # type: ignore
    constant_time_compare,
    get_random_string,
    salted_hmac,
)

from gearboxd.rpc import gearbox_pb2 as grpc  # type: ignor

# Simple regex to split titles consisting of only alphabetic characters
TITLE_SPLIT = re.compile(r"(?<!^)(?=[A-Z])")

log = logging.getLogger("gearboxlib.rpc_auth_utils")


def authorize_command(
    command: str, target: str, secret: Optional[str] = ""
) -> grpc.CommandAuthorization:
    """
    Create an ephemeral authorization object for use with the gRPC API.

    Args:
        command (str): The command for which authorization is being generated
        target (str): The target of the command, either a cog name or "daemon"
        secret (Optional[str]): Optionally specify a non-default secret to use
            Useful for calling non-local RPC servers with different secrets

    Returns:
        CommandAuthorization: A gRPC object that authorizes a specific command
    """
    secret = secret or settings.RPC_SECRET_KEY
    valid_from = datetime.utcnow().strftime(r"%Y-%m-%d %H:%M:%S")
    salt = get_random_string(16)
    return grpc.CommandAuthorization(
        token=command_hmac(command, target, valid_from, salt, secret),
        valid_from=valid_from,
        salt=salt,
    )


def check_authorization(
    command: str, target: str, auth: grpc.CommandAuthorization
) -> bool:
    """
    Check whether a command and target are currently authorized by a given
    authorization object. Imported settings are used to determine the token
    lifespan and clock drift policy.

    Args:
        command (str): The command for which authorization is being checked
        target (str): The target of the command, either a cog name or "daemon"
        auth (CommandAuthorization): A gRPC object containing authorization data

    Returns:
        bool: Whether the authorization valid
    """
    token = command_hmac(
        command, target, auth.valid_from, auth.salt, settings.RPC_SECRET_KEY
    )
    return constant_time_compare(token, auth.token) and ttl_ok(
        settings.RPC_TOKEN_LIFESPAN,
        datetime.strptime(auth.valid_from, r"%Y-%m-%d %H:%M:%S"),
        settings.RPC_AUTH_CLOCK_DRIFT_ALLOWED,
    )


def command_hmac(
    command: str, target: str, valid_from: str, salt: str, secret: str
) -> str:
    """
    Calculates a salted HMAC of a string constructed from a command, target,
    and timestamp. Imported settings are used to set the HMAC secret and
    algorithm.

    Args:
        command (str): The command that the returned HMAC will be used to authorize
        target (str): The target of the command, either a cog name or "daemon"
        valid_from (str): A datetime representation in the format %Y-%m-%d %H:%M:%S
        salt (str): A random nonce used as salt for the HMAC
        secret (str): The secret to use to compute the HMAC

    Returns:
        str: The hexadecimal hash digest string
    """
    auth_string = "_".join((command, target, valid_from)).lower()
    return salted_hmac(
        salt,
        auth_string,
        secret=secret,
        algorithm=settings.RPC_HMAC_ALGORITHM,
    ).hexdigest()


def ttl_ok(ttl: int, dtfrom: datetime, drift: int = 0) -> bool:
    """
    Checks whether a ttl is unexpired, optionally allowing for clock drift.

    Args:
        ttl (int): Time-to-live to be checked in seconds
        dtfrom (datetime): A datetime object from wich to calculate the TTL
        drift (int): Optional amount of clock drift to tolerate in seconds

    Returns:
        bool: Whether TTL still has any TTL left
    """
    return 0 - drift <= (datetime.utcnow() - dtfrom).total_seconds() <= ttl + drift


def check_auth(func, request):
    """
    Parse relevant authorization information and test it for a given request

    Intended to be wrapped by a class method decorator

    Args:
        other (object): The of the method being wrapped
        func (function): The wrapped method
        request: A gRPC request payload

    Returns:
        bool: Whether or not the request is authorized
    """
    # If command and target are undefined, use method name as hint
    split_command = TITLE_SPLIT.split(func.__name__)
    command = getattr(request, "action", split_command[0]).lower()
    target = getattr(request, "name", split_command[1]).lower()
    if not check_authorization(command, target, request.authorization):
        log.error("Received improperly authorized request!")
        return False
    return True


def _get_grpc_reply_class(func, stream: bool = False):
    """Look up gRPC return type based on endpoint called"""
    reply_class_name = f"{func.__name__}{'Stream' if stream else ''}Reply"
    return getattr(grpc, reply_class_name)


def authorized(func):
    """
    A class method decorator that implements check_auth for a gRPC servicer
    unary endpoint

    Args:
        func (function): The wrapped method

    Returns:
        gRPC response: The response from the RPC endpoint or the appropriate
        payload type with an error string
    """

    @wraps(func)
    def auth_wrap(other, request, *args, **kwargs):
        if not check_auth(func, request):
            return_type = _get_grpc_reply_class(func)
            return return_type(error="Unauthorized")
        return func(other, request, *args, **kwargs)

    return auth_wrap


def authorized_stream(func):
    """
    A class method decorator that implements check_auth for a gRPC servicer
    streaming endpoint

    Args:
        func (function): The wrapped method

    Returns:
        generator: The RPC endpoint or a dummy generator that yields the
        appropriate payload type with an error string
    """

    def _stream_responder(return_type):
        yield return_type(error="Unauthorized")

    @wraps(func)
    def auth_wrap(other, request, *args, **kwargs):
        if not check_auth(func, request):
            return_type = _get_grpc_reply_class(func, True)
            return _stream_responder(return_type)
        return func(other, request, *args, **kwargs)

    return auth_wrap

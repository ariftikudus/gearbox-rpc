import os
import sys
import time
import uuid
from collections import namedtuple
from datetime import datetime

import psutil  # type: ignore

from .logserver import Log

Status = namedtuple(
    "Status",
    "thread_pool_workers data_emitted cpu_utilization logs",
    defaults=(0, 0, 0.0, []),
)


class GearBoxAPI:
    """
    API interface to gearbox daemon, mainly useful for cog state manipulation.
    """

    def __init__(self, daemon, log_server):
        self.daemon = daemon
        self.log_server = log_server
        super().__init__()

    def list_cogs(self, request):
        cog_registrations = [x for x in self.daemon.registry.values()]
        # Normalize/format and enrich cog details
        for i, detail in enumerate(cog_registrations):
            for k, v in detail.items():
                if isinstance(v, datetime):
                    cog_registrations[i][k] = v.strftime(r"%Y-%m-%d %H:%M:%S")
            detail["loaded"] = detail["name"] in self.daemon.cogs.keys()
            detail["running"] = detail["name"] in self.daemon.running_cogs.keys()
        return cog_registrations

    def graph_cogs(self, request):
        return Graph(
            (node._asdict() for node in self.daemon.graph.nodes),
            (edge._asdict() for edge in self.daemon.graph.edges),
        )

    def watch_status(self, request, context):
        log_buffer = []
        buffer_label = uuid.uuid4()
        if not self.log_server.register(buffer_label, log_buffer):
            yield Status(
                logs=[Log(message="Too many clients, try again later")._asdict()]
            )._asdict()
            return
        proc = psutil.Process()
        while context.is_active():
            # Flush logs to a local buffer that will reset each loop
            logs = []
            if log_buffer:
                # Avoid dereferencing log_buffer
                logs, log_buffer[:] = log_buffer[:], []
            # Convert to proper Log objects
            yield Status(
                len(self.daemon.scheduler.futures),
                self.daemon.data_emitted,
                proc.cpu_percent(interval=1),
                logs=[Log(*log)._asdict() for log in logs],
            )._asdict()
            time.sleep(0.2)
        self.log_server.unregister(buffer_label)

    def cog_state_modify(self, request) -> bool:
        command_endpoint = getattr(self.daemon, f"{request.action}_cog")
        return command_endpoint(request.name)

    def reload_daemon(self, request):
        for cog in list(self.daemon.running_cogs.keys()):
            self.daemon.unload_cog(cog)

        def restart():
            # Leave time for clients to capture logs
            time.sleep(0.2)
            os.execvpe(sys.executable, [sys.executable, *sys.argv], os.environ)

        self.daemon.scheduler.run_concurrent(restart)
        return True

from collections import namedtuple
from typing import Any, Optional, Set, Tuple

from gearboxlib.templates import Cog  # type: ignore

Node = namedtuple("Node", "id label group input output")
Edge = namedtuple("Edge", "label upstream downstream")


class CogGraph:
    """Stores a network graph of cogs based on input and output models"""

    nodes: Set[Node] = set()
    edges: Set[Edge] = set()

    def add(self, cog: Cog):
        collects, emits = None, None
        if cog.collects:
            collects = cog.collects().__class__.__name__
        if cog.emits:
            emits = cog.emits().__class__.__name__
        node = Node(
            len(self.nodes) + 1,  # id
            cog.__class__.__name__,  # label
            cog._package,  # group
            collects,  # input
            emits,  # output
        )
        if node not in self.nodes:
            self.nodes.add(node)
            self._connect_node(node)

    def _connect_node(self, node: Node):
        for x in self.nodes:
            if isinstance(x.output, tuple):
                for output_model in x.output:
                    if isinstance(node.input, tuple):
                        for input_model in node.input:
                            if output_model == input_model:
                                self._connect_nodes(input_model, x, node)
                    elif output_model == node.input:
                        self._connect_nodes(output_model, x, node)
            elif isinstance(node.input, tuple):
                for input_model in node.input:
                    if x.output and x.output == input_model:
                        self._connect_nodes(x.output, x, node)
            elif x.output and x.output == node.input:
                self._connect_nodes(x.output, x, node)
            if isinstance(x.input, tuple):
                if isinstance(node.output, tuple):
                    for input_model in x.input:
                        for output_model in node.output:
                            if output_model == input_model:
                                self._connect_nodes(output_model, node, x)
                else:
                    for input_model in x.input:
                        if input_model == node.output:
                            self._connect_nodes(output_model, node, x)
            elif isinstance(node.output, tuple):
                for output_model in node.output:
                    if x.input and x.input == output_model:
                        self._connect_nodes(x.input, node, x)
            elif x.input and x.input == node.output:
                self._connect_nodes(x.input, node, x)

    def _connect_nodes(self, topic: str, node1: Node, node2: Node):
        self.edges.add(Edge(topic, node1.id, node2.id))

    def _get_node_by_id(self, node_id: int) -> Node:
        try:
            return next(filter(lambda node: node.id == node_id, self.nodes))
        except StopIteration:
            raise ValueError("Node ID is invalid")

    def get_node_by_label(self, label: str) -> Optional[Node]:
        try:
            return next(filter(lambda node: node.label == label, self.nodes))
        except StopIteration:
            return None

    def upstream(self, node: Node) -> Set[Tuple[Node, Any]]:
        return {
            (self._get_node_by_id(upstream_id), label)
            for label, upstream_id, downstream_id in self.edges
            if downstream_id == node.id
        }

    def downstream(self, node: Node) -> Set[Tuple[Node, Any]]:
        return {
            (self._get_node_by_id(downstream_id), label)
            for label, upstream_id, downstream_id in self.edges
            if upstream_id == node.id
        }

    @property
    def roots(self) -> Set[Node]:
        return {x for x in self.nodes if not x.input}

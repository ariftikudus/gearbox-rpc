import importlib
import inspect
import logging
import os
from threading import Event
from typing import Dict, List, Optional, Type

from django.conf import settings  # type: ignore
from django.db import OperationalError, close_old_connections, models  # type: ignore

from gearboxlib.models import CogMemory  # type: ignore
from gearboxlib.templates import Cog  # type: ignore

from .coggraph import CogGraph  # type: ignore
from .models import CogRegistration  # type: ignore
from .scheduler import CogScheduler  # type: ignore


class CogManager:
    log = logging.getLogger("cog_manager")
    cogs: Dict[str, Type[Cog]] = {}
    running_cogs: Dict[str, Cog] = {}
    data_emitted: int = 0
    graph: CogGraph = CogGraph()

    def __init__(self):
        self.registry = registry = CogRegistration.objects.all()
        self.scheduler = CogScheduler(self)

    def get_registration(self, cog_name: str) -> Optional[CogRegistration]:
        try:
            return CogRegistration.objects.filter(name=cog_name)[0]
        except IndexError:
            return None

    def get_or_create_registration(self, cog_name: str) -> CogRegistration:
        if not (registration := self.get_registration(cog_name)):
            registration = CogRegistration(name=cog_name)
        return registration

    def register(self, cog_class):
        self.cogs[cog_class.__name__] = cog_class
        registration = self.get_or_create_registration(cog_class.__name__)
        registration.save()
        return registration

    def _publish_historical_payloads(self, cog: Type[Cog]) -> None:
        topics: List[models.Model] = []
        if not cog.collects:
            return None
        if isinstance(cog.collects, tuple):
            topics.extend(cog.collects)
        else:
            topics.append(cog.collects)
        for topic in topics:
            try:
                latest_record = CogMemory.objects.filter(
                    cog=cog.__class__.__name__
                ).latest("recorded")
            except CogMemory.DoesNotExist:
                continue
            for payload in topic.objects.filter(recorded__gte=latest_record.recorded):
                cog._receive(payload)

    def setup_cog(self, cog) -> bool:
        self.graph.add(cog)
        if cog.setup() is False:
            self.log.error("Failed setting up cog: " + cog.__class__.__name__)
            self._set_cog_error(cog)
            return False
        # Catch the cog up on missed work
        self._publish_historical_payloads(cog)
        self.running_cogs[cog.__class__.__name__] = cog
        return True

    def allowed_to_run(self, registration: CogRegistration) -> bool:
        if not (allowed_to_run := registration.enabled and not registration.error):
            reason = "disabled" if not registration.enabled else "error state"
            self.log.debug(f"Cog {registration.name} cannot run ({reason})")
        return allowed_to_run

    def able_to_run(self, cog: Cog, registration: CogRegistration) -> bool:
        return not any(
            (
                cog.schedule is False,
                not hasattr(cog, "main"),
                not self.allowed_to_run(registration),
            )
        )

    def run(self, cog: Cog, *args):
        if not (registration := self.get_registration(cog.__class__.__name__)):
            return None
        if self.able_to_run(cog, registration):
            return self.scheduler.schedule(cog, registration, *args)

    def _set_cog_error(self, cog):
        state = self.get_or_create_registration(cog.__class__.__name__)
        state.error = True
        state.save()
        self.running_cogs.pop(cog.__class__.__name__, None)

    def clear_cog(self, name):
        return self._alter_cog_state(name, "error", False, "Cleared error for")

    def enable_cog(self, name):
        return self._alter_cog_state(name, "enabled", True, "Enabled")

    def disable_cog(self, name):
        return self.unload_cog(name) and self._alter_cog_state(
            name, "enabled", False, "Disabled"
        )

    def _alter_cog_state(self, name, key, value, description):
        # Abort if cog is not registered
        registration = self.get_registration(name)
        if not registration:
            self.log.warn(
                f"Cog {name} not registered, rejecting state change: "
                ":".join((key, str(value)))
            )
            return False
        # Make the requested state alteration
        setattr(registration, key, value)
        registration.save()
        self.log.info(" ".join((description, name)))
        return True

    def load_cog(self, name: str) -> bool:
        if not (registration := self.get_registration(name)):
            return False
        if not self.allowed_to_run(registration):
            return False
        try:
            cog_class: Cog = self.cogs[name]
        except KeyError:
            self.log.warn(f"No cog named {name} available to load")
            return False
        if name in self.running_cogs.keys():
            self.log.warn(f"Cog {name} is already loaded")
            return False
        cog = cog_class()
        if not self.setup_cog(cog):
            return False
        self.scheduler.run_concurrent(self.run, cog)
        self.log.info(f"Loaded cog {name}")
        return True

    def unload_cog(self, name: str) -> bool:
        try:
            cog: Cog = self.running_cogs[name]
        except KeyError:
            self.log.warn(f"No cog named {name} available to unload")
            return True
        self.scheduler.interrupts.pop(name, Event()).set()
        cog.unsetup()
        del self.running_cogs[name]
        self.log.info(f"Unloaded {name}")
        return True

    def reload_cog(self, name: str) -> bool:
        try:
            cog_class: Cog = self.cogs[name]
        except KeyError:
            self.log.warn(f"No cog named {name} available to load")
            return False
        module = inspect.getmodule(cog_class)
        if not module:
            self.log.warn(
                f"Failed reloading cog package {cog_class.__module__} for {name}"
            )
            return False
        package_name = module.__file__.split(os.sep)[-2]
        importlib.reload(module)
        self.log.info(f"Reloaded cog package {package_name}")
        cog_class = getattr(module, name)
        cog_class._package = package_name
        self.cogs[name] = cog_class
        self.unload_cog(name)
        return self.load_cog(name)

    def remove_cog(self, cog_name: str) -> bool:
        registration = self.get_registration(cog_name)
        if not registration:
            return False
        registration.delete()
        return True

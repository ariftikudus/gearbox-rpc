import time

init_time = time.time()

import logging
import os
from concurrent.futures import ThreadPoolExecutor
from typing import Dict, List

import django  # type: ignore
import grpc  # type: ignore
import yaml  # type: ignore
from django.conf import settings  # type: ignore
from django.db import OperationalError, close_old_connections  # type: ignore
from protection import protect  # type: ignore
from pubsub import pub  # type: ignore

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gearboxweb.settings")

django.setup()

from gearboxlib.cog_utils import find_packages, load_cogs  # type: ignore
from gearboxlib.templates import Cog  # type: ignore

from .api import GearBoxAPI  # type: ignore
from .depgraph import COMPARATORS, DependencyGraph, Package, Version  # type: ignore

try:
    from .gbrpc import GearBoxRPC  # type: ignore
    from .rpc import gearbox_pb2_grpc  # type: ignore
except ImportError:
    # Will deal with this during setup phase
    pass

from .logserver import LogBufferServer  # type: ignore
from .manager import CogManager  # type: ignore
from .models import CogRegistration  # type: ignore

import_init_time = time.time()

APP = "GearBox"
VERSION = Version(0, 7, 0)  # major, minor, patch

# Lookup table for comparison operator strings
COMPARATOR_STRINGS = {y: x for x, y in COMPARATORS.items()}

# Set a log client limit that leaves some workers free for unary RPC commands
LOG_BUFFER_SERVER = LogBufferServer(max((settings.RPC_WORKERS - 2, 1)))


class GearBox:
    log = logging.getLogger("gearbox")
    packages: Dict[str, Dict] = {}

    def __init__(self):
        # Start performance timer for init process
        self.class_init_start = time.time()
        # Reigster LogBufferWriter server so remote clients can read logs
        self.log.addHandler(LOG_BUFFER_SERVER)
        # Set up PID protection
        self.p = protect(name=APP.lower(), max_age=60000000)
        # Set up cog manager
        self.manager = CogManager()
        self.manager.log.addHandler(LOG_BUFFER_SERVER)
        # Set up API class with reference to cog manager
        api = GearBoxAPI(self.manager, LOG_BUFFER_SERVER)
        # Try to start the RPC server
        try:
            handler = GearBoxRPC(api)
            self.server = grpc.server(
                ThreadPoolExecutor(max_workers=settings.RPC_WORKERS)
            )
            gearbox_pb2_grpc.add_GearBoxServicer_to_server(handler, self.server)
            self.server.add_insecure_port(settings.RPC_BIND_INTERFACE)
            self.server.start()
        except NameError as e:
            self.server = None
            self.log.error(
                "RPC server module not found. Did you build the gRPC client?"
            )
        self.log.info(f"Starting {APP} v{'.'.join(str(x) for x in VERSION)}")
        pub.subscribe(self.record, pub.ALL_TOPICS)
        self.class_init_end = time.time()

    def exit(self):
        if self.server:
            self.server.stop(0)
        self.p.cleanup()
        exit(0)

    def resolve_dependencies(self):
        # Collect package manifests
        for pack_path in find_packages(settings.PACKAGES_PATH):
            pack_name = pack_path.split(os.sep)[-2]
            if pack_name in self.packages.keys():
                self.log.error(f"Can't load multiple packages named {pack_name}")
                continue
            try:
                self.packages[pack_name] = yaml.safe_load(open(pack_path)) or {}
            except Exception as e:
                self.log.exception(f"Failed to load package: {pack_name}", exc_info=e)
                continue
        else:
            self.log.info(f"No packages found in path: {settings.PACKAGES_PATH}")

        # Initialize dependency graph model
        depGraph = DependencyGraph()
        depGraph.add(APP, VERSION)

        # Check package requirements
        for pack, manifest in self.packages.items():
            if requirements := manifest.get("requirements", False):
                gbreq = APP
                if gbminv := requirements.get("gearbox_min", False):
                    gbreq = f"{gbminv}<={gbreq}"
                if gbmaxv := requirements.get("gearbox_max", False):
                    gbreq = f"{gbreq}<={gbmaxv}"
                try:
                    depGraph.link(pack, [gbreq, *requirements.get("packages", [])])
                except Exception as e:
                    self.log.warn(f"{pack} has bogus requirements", exc_info=e)
            pack_version = manifest.get("version", 0)
            depGraph.add(pack, pack_version)

        # Resolve dependency relationships
        success, failure = depGraph.resolve()

        # Report packages which failed to load
        for pack in {x.required_by for x in failure}:
            error = f"Not loading {pack} because it requires: "
            reasons = [x for x in failure if x.required_by == pack]
            reason_strings = []
            for _, requires, comparator, version in reasons:
                c_string = COMPARATOR_STRINGS[comparator]
                v_string = ".".join(str(x) for x in version)
                reason_strings.append(f"{requires} {c_string} {v_string}")
            self.log.error(f"{error}{', '.join(reason_strings)}")

        # Remove GearBox from nodes because it is not actually a package
        depGraph.nodes.remove(Package(APP, VERSION))
        return success

    def load(self):
        # Quit if there's no work to be done
        if not (success := self.resolve_dependencies()):
            self.log.fatal("No cog packages passed dependency checks.")
            self.exit()

        # Load cog packages whose dependencies have been met and have exports
        packages = {
            x.name: self.packages[x.name]["exports"]
            for x in success
            if self.packages[x.name].get("exports")
        }
        # Filter out cogs with duplicate names
        exports: List[str] = []
        for export_list in packages.values():
            exports.extend(export_list or [])
        unique_exports = [x for x in exports if exports.count(x) == 1]
        if len(exports) != len(unique_exports):
            duplicate_exports = {x for x in exports if x not in unique_exports}
            self.log.error(
                f"Unable to load cogs with duplicate names: {', '.join(duplicate_exports)}"
            )
            # Filter duplicate cog names out of packages
            for pack, cogs in packages.items():
                for cog_name in cogs.copy():
                    if cog_name in duplicate_exports:
                        packages[pack].remove(cog_name)
        for cog_class in load_cogs(settings.PACKAGES_PATH, packages):
            try:
                # Register cog to retrieve or initialize cog state information
                registration: CogRegistration = self.manager.register(cog_class)
                # Check whether registration allows cog to run
                if not self.manager.allowed_to_run(registration):
                    continue
                # Initialize and set up the cog
                cog = cog_class()
                self.manager.setup_cog(cog)
            except Exception as e:
                self.log.exception(
                    f"Caught unhandled exception setting up cog {cog_class.__name__}"
                )
        cog_load_end = time.time()
        self.log.debug(
            "Startup timings:"
            f" import:{round(import_init_time-init_time, 5)}"
            f" init:{round(self.class_init_end-self.class_init_start, 5)}"
            f" load cogs:{round(cog_load_end-self.class_init_end, 5)}"
            f" total:{round(cog_load_end-init_time, 5)}"
        )
        del self.class_init_start
        del self.class_init_end

    def run(self) -> None:
        try:
            self.manager.scheduler.main()
        except (KeyboardInterrupt, SystemExit):
            for interrupt in self.manager.scheduler.interrupts.values():
                interrupt.set()
            for future in self.manager.scheduler.futures:
                future.cancel()
            self.manager.scheduler.pool.shutdown()
        except Exception as e:
            self.log.exception("Oops", exc_info=e)
        finally:
            self.exit()
        Pool is closed

    def record(self, **kwargs):
        payload = kwargs["payload"]
        try:
            payload.save()
        except OperationalError:
            # Sometimes the database disconnects
            close_old_connections()
            payload.save()
        self.manager.data_emitted += 1

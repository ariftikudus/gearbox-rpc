import time
import logging
from concurrent.futures import (FIRST_COMPLETED, Future, ThreadPoolExecutor,
                                wait)
from datetime import datetime, timezone
from threading import Event
from typing import Dict, List

from pubsub import pub  # type: ignore

from gearboxlib.models import CogMemory  # type: ignore
from gearboxlib.templates import Cog  # type: ignore

from .models import CogRegistration  # type: ignore


class CogScheduler:
    log = logging.getLogger("cog_scheduler")
    pool: ThreadPoolExecutor
    futures: List[Future] = []
    interrupts: Dict[str, Event] = {}

    def __init__(self, manager):
        self.manager = manager

    def main(self):
        # Set up interrupt event for self
        self.interrupts["daemon"] = Event()
        # Publishers must imeplement a main() method
        publishers = [
            x for x
            in self.manager.running_cogs.values()
            if hasattr(x, "main")
        ]
        # Start concurrent thread pool
        with ThreadPoolExecutor() as self.pool:
            # Start publisher cogs
            self.futures = [
                self.pool.submit(self.manager.run, cog)
                for cog in publishers
            ]
            # Create tracking list for dequeued jobs and their schedule backoffs
            dequeued = []
            # Set up forever loop
            while True:
                # Await completion of any future
                while self.futures:
                    dequeue_time = time.time()
                    for finished in wait(
                        self.futures.copy(), return_when=FIRST_COMPLETED
                    ).done:
                        if e := finished.exception():
                            self.log.exception("Unhandled cog exception", exc_info=e)
                            continue
                        self.futures.remove(finished)
                        result = finished.result()
                        if not result:
                            continue
                        dequeued.append(result)
                    if not dequeued:
                        continue
                    dequeued = self.requeue(dequeued, dequeue_time, max_wait=0.05)
                # and then keep waiting, nobody said you could stop waiting
                if not dequeued:
                    sleep(0.5)
                    continue
                dequeued = self.requeue(dequeued, dequeue_time)

    def run_concurrent(self, function, *args, **kwargs):
        self.futures.append(self.pool.submit(function, *args, **kwargs))

    def requeue(self, dequeued, dequeue_time, max_wait=1000, min_wait=0.0001):
        backoff = min(backoff for _, backoff in dequeued)
        sleep_wait = max((min((backoff, max_wait)), min_wait))
        if self.interrupts["daemon"].wait(sleep_wait):
            print("Interrupted")
            return
        # Add bias offset to err on the side of punctuality
        interval = round(time.time() - dequeue_time, 4)
        # print(sleep_wait, interval, len(dequeued))
        for i, (cog, backoff) in enumerate(dequeued):
            if backoff <= interval:
                self.run_concurrent(self.manager.run, cog)
                dequeued.remove((cog, backoff))
            else:
                dequeued[i] = (cog, backoff - interval)
        return dequeued

    def _spawn_worker(self, cog: Cog, interrupt: Event):
        try:
            result = cog.main(interrupt)
        except Exception as e:
            self.log.error("Exception running cog", exc_info=e)
            self.manager._set_cog_error(cog)
            return False
        if result is False:
            self.log.error(f"{cog.__class__.__name__} crashed, setting error flag")
            self.manager._set_cog_error(cog)
            return False
        return cog

    def schedule(self, cog: Cog, registration: CogRegistration, interrupt: Event = None):
        # Create and track interrupt signal for cog if not defined
        interrupt = interrupt or Event()
        self.interrupts[registration.name] = interrupt
        if registration.last_run:
            # Calculate time until next scheduled run
            backoff = round(
                (registration.last_run - datetime.now(tz=timezone.utc)).total_seconds()
                + cog.schedule,
                4,
            )
            if backoff > 0:
                return cog, backoff
        # Update last_run value
        registration.last_run = datetime.now(tz=timezone.utc)
        result = self._spawn_worker(cog, interrupt)
        registration.save()
        return result, 0

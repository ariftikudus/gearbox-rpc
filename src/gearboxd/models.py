from django.db import models  # type: ignore


class CogRegistration(models.Model):
    name = models.CharField(max_length=128, primary_key=True)
    enabled = models.BooleanField(default=True)
    error = models.BooleanField(default=False)
    last_run = models.DateTimeField(auto_now=True)

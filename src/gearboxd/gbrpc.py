from gearboxlib.utils import authorized, authorized_stream  # type: ignore

from .rpc import gearbox_pb2 as grpc  # type: ignore
from .rpc.gearbox_pb2_grpc import GearBoxServicer  # type: ignore


class GearBoxRPC(GearBoxServicer):
    """
    gRPC interface to the GearBox API

    All operations must be authorized by computing an HMAC of the
    command, target (cog name or "daemon"), and current timestamp, along with a
    random nonce for salt and the Django SECRET_KEY as the signing secret.
    """

    def __init__(self, api):
        self.api = api

    @authorized
    def ListCogs(self, request, _):
        """
        Calls the list_cogs API endpoint

        Args:
            request (grpc.ListCogsRequest): Contains only an auth payload
            _ (): Request context, not used

        Returns:
            grpc.ListCogsReply: API response packaged for gRPC
        """
        return grpc.ListCogsReply(
            cog_registry=[
                grpc.ListCogsReply.CogDetails(**cog)
                for cog in self.api.list_cogs(request)
            ],
        )

    @authorized
    def GraphCogs(self, request, _):
        """
        Calls the graph_cogs API endpoint

        Args:
            request (grpc.GraphCogsRequest): Contains only an auth payload
            _ (): Request context, not used

        Returns:
            grpc.GraphCogsReply: API response packaged for gRPC
        """
        graph = self.api.graph_cogs(request)
        N, E = grpc.GraphCogsReply.GraphNode, grpc.GraphCogsReply.GraphEdge
        return grpc.GraphCogsReply(
            nodes=[N(**node) for node in graph.nodes],
            edges=[E(**edge) for edge in graph.edges],
        )

    @authorized_stream
    def WatchStatus(self, request, context):
        """
        Calls the watch_status API generator endpoint

        Args:
            request (grpc.WatchStatusRequest): Contains only an auth payload
            context (): Request context, used to monitor stream client state

        Yields:
            Generator[grpc.WatchStatusStreamReply, None, None]: API response packaged for gRPC
        """
        for status in self.api.watch_status(request, context):
            # Encapsulate logs in proper data type
            L = grpc.WatchStatusStreamReply.Log
            status["logs"] = [L(**log) for log in status["logs"]]
            yield grpc.WatchStatusStreamReply(**status)

    @authorized
    def CogStateModify(self, request, _):
        """
        Calls the cog_state_modify API endpoint

        Args:
            request (grpc.CogStateModifyRequest): Contains a target name, action, and auth payload
            _ (): Request context, not used

        Returns:
            grpc.CogStateModifyReply: API response packaged for gRPC
        """
        return grpc.CogStateModifyReply(success=self.api.cog_state_modify(request))

    @authorized
    def ReloadDaemon(self, request, _):
        """
        Calls the reload_daemon API endpoint

        Args:
            request (grpc.ReloadDaemonRequest): Contains only an auth payload
            _ (): Request context, not used

        Returns:
            grpc.ReloadDaemonReply: API response packaged for gRPC
        """
        return grpc.ReloadDaemonReply(success=self.api.reload_daemon(request))

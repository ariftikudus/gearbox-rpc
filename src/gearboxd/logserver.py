import logging
import time
from collections import namedtuple

from typing import Dict, List

log = logging.getLogger("log_buffer_server")

Log = namedtuple(
    "Log", "context level message timestamp", defaults=("error", "critical", "", "-")
)

class LogBufferServer(logging.StreamHandler):
    buffers: Dict[str, List[Log]] = {}

    def __init__(self, max_clients: int = 1):
        self.max_clients = max_clients
        super().__init__()
        log.addHandler(self)

    def register(self, label: str, buffer: list) -> bool:
        if len(self.buffers) >= self.max_clients:
            log.warn("Max log clients reached, rejecting new client")
            return False
        self.buffers[label] = buffer
        log.debug("Log buffer client attached")
        return True

    def unregister(self, label: str):
        del self.buffers[label]
        log.debug("Log buffer client detached")

    def emit(self, record):
        message = self.format(record)
        timestamp = time.asctime(time.localtime())
        asctime = f"{timestamp[-4:]} {timestamp[:-5]},{record.msecs}"
        for buffer in self.buffers.values():
            buffer.append(Log(record.name, record.levelname, message, asctime))
